package xyz.bringoff.todolight.ui.taskdetails;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import xyz.bringoff.todolight.data.Task;
import xyz.bringoff.todolight.data.TasksRepository;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class TaskDetailsViewModelTest {

    private static final String TASK_UUID = "some_uuid";

    @Rule
    public InstantTaskExecutorRule instantExecutor = new InstantTaskExecutorRule();

    @Mock
    Observer<Task> mockTaskObserver;
    @Mock
    Observer<TaskDetailsWarnings> mockWarningObserver;
    @Mock
    Observer<Void> mockExitObserver;
    @Mock
    TasksRepository mockTasksRepository;

    private TaskDetailsViewModel testingObject;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldWarnUser_IfTriesToExitWithoutSavingNewCompletionState() {
        Task stubTask = new Task(TASK_UUID, "some description", false);
        MutableLiveData<Task> stubLiveTask = new MutableLiveData<>();
        stubLiveTask.setValue(stubTask);
        when(mockTasksRepository.getTask(anyString())).thenReturn(stubLiveTask);

        testingObject = new TaskDetailsViewModel(mockTasksRepository, TASK_UUID);
        testingObject.getTask().observeForever(mockTaskObserver);
        testingObject.exitScreenNavigationCommand.observeForever(mockExitObserver);
        testingObject.warningCommand.observeForever(mockWarningObserver);

        testingObject.onChangeCompletionStatusRequested(true);
        testingObject.onExitRequested();
        assertEquals(TaskDetailsWarnings.CHANGES_NOT_SAVED, testingObject.warningCommand.getValue());
    }

    @Test
    public void shouldWarnUser_IfTriesToExitWithoutSavingNewDescription() {
        Task stubTask = new Task(TASK_UUID, "some description", false);
        MutableLiveData<Task> stubLiveTask = new MutableLiveData<>();
        stubLiveTask.setValue(stubTask);
        when(mockTasksRepository.getTask(anyString())).thenReturn(stubLiveTask);

        testingObject = new TaskDetailsViewModel(mockTasksRepository, TASK_UUID);
        testingObject.getTask().observeForever(mockTaskObserver);
        testingObject.exitScreenNavigationCommand.observeForever(mockExitObserver);
        testingObject.warningCommand.observeForever(mockWarningObserver);

        testingObject.onChangeDescriptionRequested("changed description");
        testingObject.onExitRequested();
        assertEquals(TaskDetailsWarnings.CHANGES_NOT_SAVED, testingObject.warningCommand.getValue());
    }

    @Test
    public void shouldNotWarnUser_IfTriesToExitAfterSavingNewCompletionState() {
        Task stubTask = new Task(TASK_UUID, "some description", false);
        MutableLiveData<Task> stubLiveTask = new MutableLiveData<>();
        stubLiveTask.setValue(stubTask);
        when(mockTasksRepository.getTask(anyString())).thenReturn(stubLiveTask);

        testingObject = new TaskDetailsViewModel(mockTasksRepository, TASK_UUID);
        testingObject.exitScreenNavigationCommand.observeForever(mockExitObserver);
        testingObject.warningCommand.observeForever(mockWarningObserver);

        testingObject.onChangeCompletionStatusRequested(true);
        testingObject.onSaveTaskRequested();

        testingObject.onExitRequested();
        assertEquals(null, testingObject.warningCommand.getValue());
    }

    @Test
    public void shouldNotWarnUser_IfTriesToExitAfterSavingNewDescription() {
        Task stubTask = new Task(TASK_UUID, "some description", false);
        MutableLiveData<Task> stubLiveTask = new MutableLiveData<>();
        stubLiveTask.setValue(stubTask);
        when(mockTasksRepository.getTask(anyString())).thenReturn(stubLiveTask);

        testingObject = new TaskDetailsViewModel(mockTasksRepository, TASK_UUID);
        testingObject.exitScreenNavigationCommand.observeForever(mockExitObserver);
        testingObject.warningCommand.observeForever(mockWarningObserver);

        testingObject.onChangeDescriptionRequested("changed description");
        testingObject.onSaveTaskRequested();

        testingObject.onExitRequested();
        assertEquals(null, testingObject.warningCommand.getValue());
    }
}