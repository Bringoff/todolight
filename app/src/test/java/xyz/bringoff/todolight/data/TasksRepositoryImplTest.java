package xyz.bringoff.todolight.data;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import xyz.bringoff.todolight.util.SingleExecutors;
import xyz.bringoff.todolight.data.db.TaskDao;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


public class TasksRepositoryImplTest {

    @Rule
    public InstantTaskExecutorRule instantExecutor = new InstantTaskExecutorRule();

    @Mock
    TaskDao mockTaskDao;

    private TasksRepositoryImpl testingClass;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        testingClass = new TasksRepositoryImpl(new SingleExecutors(), mockTaskDao);
    }

    @Test
    public void shouldSaveNewTaskCorrectly() {
        Task taskToInsert = new Task("uuid", "task name", false);
        testingClass.putTask(taskToInsert);
        verify(mockTaskDao).insertTasks(any());
    }

    @Test
    public void shouldSaveMultipleTasksCorrectly() {
        Task task1 = new Task("uuid", "task name", false);
        Task task2 = new Task("uuid2", "another task name", true);
        Task task3 = new Task("some andother uuid3", "third task name", false);
        testingClass.putTask(task1);
        testingClass.putTask(task2);
        testingClass.putTask(task3);
        verify(mockTaskDao, times(3)).insertTasks(any());
    }

    @Test
    public void shouldRemoveExistingTask() {
        Task existingTask = new Task("uuid", "name", false);
        testingClass.removeTask(existingTask);
        verify(mockTaskDao).deleteTasks(any());
    }
}