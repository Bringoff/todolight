package xyz.bringoff.todolight;

import android.app.Application;
import android.arch.persistence.room.Room;

import toothpick.config.Module;
import xyz.bringoff.todolight.data.TasksRepository;
import xyz.bringoff.todolight.data.TasksRepositoryImpl;
import xyz.bringoff.todolight.data.db.AppDatabase;
import xyz.bringoff.todolight.data.db.TaskDao;
import xyz.bringoff.todolight.data.db.TaskDaoProvider;
import xyz.bringoff.todolight.util.AppExecutors;

public class AppModule extends Module {
    private Application mApplication;

    public AppModule(Application application) {
        mApplication = application;

        bind(TasksRepository.class).to(TasksRepositoryImpl.class);
        bind(AppDatabase.class).toProviderInstance(() ->
                Room.databaseBuilder(mApplication,
                        AppDatabase.class, AppDatabase.DATABASE_NAME)
                        .build());
        bind(TaskDao.class).toProvider(TaskDaoProvider.class);
        bind(AppExecutors.class).toInstance(new AppExecutors());
    }
}
