package xyz.bringoff.todolight.util;

import android.support.annotation.NonNull;
import android.widget.CompoundButton;

import java.lang.reflect.Field;

public class ViewUtil {

    /**
     * {@link CompoundButton#setChecked(boolean)} method triggers {@link CompoundButton.OnCheckedChangeListener}
     * which is not good for some cases. So this method uses workaround to remove that listener
     * temporarily and put it back after checked status change.
     * It uses reflection, so it has fallback to regular {@link CompoundButton#setChecked(boolean)}
     * method call if reflection exception is thrown, but usually it should not happen.
     */
    public static void setCheckedWithoutCallingListener(@NonNull CompoundButton compoundButton,
                                                        boolean isChecked) {
        try {
            Field listenerField = CompoundButton.class.getDeclaredField("mOnCheckedChangeListener");
            listenerField.setAccessible(true);
            CompoundButton.OnCheckedChangeListener listenerValue =
                    (CompoundButton.OnCheckedChangeListener) listenerField.get(compoundButton);

            compoundButton.setOnCheckedChangeListener(null);
            compoundButton.setChecked(isChecked);
            compoundButton.setOnCheckedChangeListener(listenerValue);
        } catch (IllegalAccessException e) {
            compoundButton.setChecked(isChecked);
        } catch (NoSuchFieldException e) {
            compoundButton.setChecked(isChecked);
        }
    }
}
