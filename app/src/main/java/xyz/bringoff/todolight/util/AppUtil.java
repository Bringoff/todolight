package xyz.bringoff.todolight.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

public class AppUtil {

    public static void restartApp(@NonNull Context context) {
        Intent restartIntent = context.getPackageManager()
                .getLaunchIntentForPackage(context.getPackageName());
        if (restartIntent == null) {
            return;
        }
        restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        restartIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent intent = PendingIntent.getActivity(context, 111,
                restartIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (manager != null) {
            manager.set(AlarmManager.RTC, System.currentTimeMillis() + 1, intent);
            System.exit(0);
        }
    }
}
