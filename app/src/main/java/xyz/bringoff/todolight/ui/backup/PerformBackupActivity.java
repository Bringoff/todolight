package xyz.bringoff.todolight.ui.backup;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.MetadataChangeSet;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.inject.Inject;

import xyz.bringoff.todolight.R;
import xyz.bringoff.todolight.data.db.AppDatabase;

public class PerformBackupActivity extends BaseDriveBackupActivity {

    private static final String TAG = "RestoreFromBackupAct...";

    @Inject
    AppDatabase mAppDatabase;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, PerformBackupActivity.class);
    }

    @Override
    protected void onDriveClientReady() {
        putDatabaseToAppFolder(mAppDatabase.getOpenHelper().getReadableDatabase());
    }

    private void putDatabaseToAppFolder(SupportSQLiteDatabase database) {
        final Task<DriveFolder> appFolderTask = getDriveResourceClient().getAppFolder();
        final Task<DriveContents> createContentsTask = getDriveResourceClient().createContents();

        Tasks.whenAll(appFolderTask, createContentsTask)
                .continueWithTask(task -> {
                    DriveFolder parent = appFolderTask.getResult();
                    DriveContents contents = createContentsTask.getResult();

                    // write content to DriveContents
                    OutputStream outputStream = contents.getOutputStream();

                    FileInputStream inputStream = null;
                    try {
                        inputStream = new FileInputStream(new File(database.getPath()));
                    } catch (FileNotFoundException e) {
                        Log.e(TAG, "Error performing backup", e);
                        showMessage(getString(R.string.backup_performing_failed));
                    }

                    byte[] buf = new byte[1024];
                    int bytesRead;
                    try {
                        if (inputStream != null) {
                            while ((bytesRead = inputStream.read(buf)) > 0) {
                                outputStream.write(buf, 0, bytesRead);
                            }
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Error performing backup", e);
                        showMessage(getString(R.string.backup_performing_failed));
                    }


                    MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                            .setTitle(buildBackupTitle())
                            .setMimeType(DATABASE_MIMETYPE)
                            .build();

                    return getDriveResourceClient().createFile(parent, changeSet, contents);
                })
                .addOnSuccessListener(this,
                        driveFile -> {
                            showMessage(getString(R.string.backup_performed_successfully));
                            finish();
                        })
                .addOnFailureListener(this, e -> {
                    Log.e(TAG, "Unable to create file", e);
                    showMessage(getString(R.string.backup_performing_failed));
                    finish();
                });
    }

    private String buildBackupTitle() {
        return new Date().toString();
    }
}
