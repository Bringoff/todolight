package xyz.bringoff.todolight.ui.backup;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.widget.DataBufferAdapter;

interface OnBackupClickListener {

    void onBackupEntryClick(@NonNull DriveId backupId);
}

public class BackupsAdapter extends DataBufferAdapter<Metadata> {

    @Nullable
    private OnBackupClickListener mBackupClickListener;

    public BackupsAdapter(Context context, @Nullable OnBackupClickListener backupClickListener) {
        super(context, android.R.layout.simple_list_item_1);
        mBackupClickListener = backupClickListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(getContext(), android.R.layout.simple_list_item_1, null);
        }
        Metadata metadata = getItem(position);
        convertView.setClickable(true);
        convertView.setOnClickListener(v -> {
            if (mBackupClickListener != null) {
                mBackupClickListener.onBackupEntryClick(metadata.getDriveId());
            }
        });
        TextView titleTextView = convertView.findViewById(android.R.id.text1);
        titleTextView.setText(metadata.getTitle());
        return convertView;
    }
}