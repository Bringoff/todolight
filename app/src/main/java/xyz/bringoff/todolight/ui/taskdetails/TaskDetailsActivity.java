package xyz.bringoff.todolight.ui.taskdetails;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import xyz.bringoff.todolight.R;
import xyz.bringoff.todolight.ui.BaseActivity;
import xyz.bringoff.todolight.util.ViewUtil;

public class TaskDetailsActivity extends BaseActivity {

    private static final String EXTRA_TASK_UUID = "xyz.bringoff.todolight.TASK_UUID";

    private TaskDetailsViewModel mViewModel;

    private Toolbar mToolbar;
    private EditText mTaskDescription;
    private CheckBox mTaskCompletedStatus;

    public static Intent getViewDetailsIntent(@NonNull Context context, @NonNull String existingTaskUuid) {
        Intent startIntent = new Intent(context, TaskDetailsActivity.class);
        startIntent.putExtra(EXTRA_TASK_UUID, existingTaskUuid);
        return startIntent;
    }

    public static Intent getCreateNewIntent(@NonNull Context context) {
        return new Intent(context, TaskDetailsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

        initViews();
        initViewModel();
        observeViewModel();
    }

    private void initViews() {
        mTaskDescription = findViewById(R.id.task_details_description);
        mTaskDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mViewModel.onChangeDescriptionRequested(s.toString());
            }
        });

        mTaskCompletedStatus = findViewById(R.id.task_details_is_completed);
        mTaskCompletedStatus.setOnCheckedChangeListener(
                (buttonView, isChecked) -> mViewModel.onChangeCompletionStatusRequested(isChecked));

        mToolbar = findViewById(R.id.task_details_toolbar);
        mToolbar.inflateMenu(R.menu.menu_task_details);
        mToolbar.setNavigationIcon(R.drawable.ic_back_light);
        mToolbar.setNavigationOnClickListener(view -> mViewModel.onExitRequested());
        mToolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.task_details_menu_save:
                    mViewModel.onSaveTaskRequested();
                    return true;
                case R.id.task_details_menu_delete:
                    mViewModel.onDeleteTaskRequested();
                    return true;
            }
            return false;
        });
    }

    private void initViewModel() {
        String providedTaskUuid = getIntent().getStringExtra(EXTRA_TASK_UUID);
        mViewModel = ViewModelProviders
                .of(this, new TaskDetailsViewModelFactory(getApplication(), providedTaskUuid))
                .get(TaskDetailsViewModel.class);
    }

    private void observeViewModel() {
        mViewModel.getTask().observe(this, task -> {
            if (task != null) {
                mTaskDescription.setText(task.getDescription());
                ViewUtil.setCheckedWithoutCallingListener(mTaskCompletedStatus, task.isCompleted());
            }
        });
        mViewModel.exitScreenNavigationCommand.observe(this, v -> TaskDetailsActivity.this.finish());
        mViewModel.warningCommand.observe(this, taskDetailsWarnings -> {
            assert taskDetailsWarnings != null;
            switch (taskDetailsWarnings) {
                case CHANGES_NOT_SAVED:
                    Toast.makeText(this,
                            R.string.task_details_not_saved_warning, Toast.LENGTH_LONG)
                            .show();
                    break;
            }
        });
    }

    @Override
    public void onBackPressed() {
        mViewModel.onExitRequested();
    }
}
