package xyz.bringoff.todolight.ui.backup;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.events.OpenFileCallback;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.drive.widget.DataBufferAdapter;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import xyz.bringoff.todolight.R;
import xyz.bringoff.todolight.util.AppUtil;

public class RestoreFromBackupActivity extends BaseDriveBackupActivity implements OnBackupClickListener {

    private static final String TAG = "RestoreFromBackupAct...";


    private RestoreFromBackupViewModel mViewModel;
    private DataBufferAdapter<Metadata> mBackupsAdapter;

    private ProgressBar mProgressBar;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, RestoreFromBackupActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backup);

        mProgressBar = findViewById(R.id.backup_restore_progress);

        ListView mBackupsListView = findViewById(R.id.backups_list);
        mBackupsAdapter = new BackupsAdapter(this, this);
        mBackupsListView.setAdapter(mBackupsAdapter);
        mBackupsListView.setEmptyView(findViewById(R.id.backups_list_empty));

        mViewModel = ViewModelProviders.of(this,
                new RestoreFromBackupViewModelFactory(getApplication()))
                .get(RestoreFromBackupViewModel.class);
        mViewModel.getRestoringFromBackupError().observe(this, this::showMessage);
        mViewModel.getRestoringProgressStatus().observe(this, inProgress -> {
            if (inProgress == Boolean.TRUE) {
                mProgressBar.setVisibility(View.VISIBLE);
            } else {
                mProgressBar.setVisibility(View.GONE);
            }
        });
        mViewModel.getRestoringFromBackupSuccessful().observe(this,
                nothing -> {
                    showMessage(getString(R.string.backup_restore_performed_successfully));
                    restartApplication();
                });
    }

    @Override
    protected void onDriveClientReady() {
        DriveClient driveClient = Drive.getDriveClient(this, getGoogleSignInAccount());
        Task<Void> syncTask = driveClient.requestSync();

        Task<DriveFolder> loadAppFolder = getDriveResourceClient().getAppFolder();

        Tasks.whenAll(syncTask, loadAppFolder)
                .addOnSuccessListener(aVoid -> listFilesInFolder(loadAppFolder.getResult()))
                .addOnFailureListener(e -> showMessage(getString(R.string.backup_files_query_failed)));

    }

    @Override
    protected void onStop() {
        super.onStop();
        mBackupsAdapter.clear();
    }

    /**
     * Retrieves results for the next page. For the first run,
     * it retrieves results for the first page.
     */
    private void listFilesInFolder(DriveFolder folder) {
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.MIME_TYPE, DATABASE_MIMETYPE))
                .build();

        Task<MetadataBuffer> queryTask = getDriveResourceClient().queryChildren(folder, query);

        queryTask.addOnSuccessListener(
                this, metadataBuffer -> mBackupsAdapter.append(metadataBuffer))
                .addOnFailureListener(
                        this, e -> {
                            Log.e(TAG, "Error retrieving files", e);
                            showMessage(getString(R.string.backup_files_query_failed));
                        });
    }

    @Override
    public void onBackupEntryClick(@NonNull DriveId backupId) {
        retrieveBackupFile(backupId.asDriveFile());
    }

    private void retrieveBackupFile(DriveFile file) {
        mProgressBar.setVisibility(View.VISIBLE);
        OpenFileCallback openCallback = new OpenFileCallback() {
            @Override
            public void onProgress(long bytesDownloaded, long bytesExpected) {
                int progress = (int) (bytesDownloaded * 100 / bytesExpected);
                Log.d(TAG, String.format("Loading progress: %d percent", progress));
                mProgressBar.setProgress(progress);
            }

            @Override
            public void onContents(@NonNull DriveContents driveContents) {
                replaceLocalDatabaseWithBackup(driveContents);
            }

            private void replaceLocalDatabaseWithBackup(@NonNull DriveContents driveContents) {
                mViewModel.onRestoreDatabaseBackupRequested(driveContents.getInputStream());
            }

            @Override
            public void onError(@NonNull Exception e) {
                Log.e(TAG, "Unable to read contents", e);
                showMessage(getString(R.string.backup_restore_performing_failed));
                mProgressBar.setVisibility(View.GONE);
            }
        };

        getDriveResourceClient().openFile(file, DriveFile.MODE_READ_ONLY, openCallback);
    }

    private void restartApplication() {
        // TODO: 12/30/17 this is the easiest way to force the app to reload all db-related stuff.
        // But it is not the smartest. Maybe, find another solution.
        RestoreFromBackupActivity.this.runOnUiThread(
                () -> AppUtil.restartApp(RestoreFromBackupActivity.this));
    }
}
