package xyz.bringoff.todolight.ui.tasklist;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import xyz.bringoff.todolight.R;
import xyz.bringoff.todolight.data.Task;
import xyz.bringoff.todolight.util.ViewUtil;

import static android.support.v7.util.DiffUtil.calculateDiff;

public class TasksAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Task> mTasks = Collections.emptyList();

    @Nullable
    private OnTaskClickListener mTaskClickListener;
    @Nullable
    private OnTaskCompletionListener mTaskCompletionListener;

    public TasksAdapter() {
        this(null, null);
    }

    public TasksAdapter(@Nullable OnTaskClickListener taskClickListener,
                        @Nullable OnTaskCompletionListener taskCompletionListener) {
        mTaskClickListener = taskClickListener;
        mTaskCompletionListener = taskCompletionListener;
    }

    public void setNewTasks(@NonNull final List<Task> newTasks) {
        final List<Task> oldTasks = mTasks;
        DiffUtil.DiffResult diffResult = calculateDiff(new TasksDiffUtilsCallback(oldTasks, newTasks));
        mTasks = newTasks;
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater.inflate(R.layout.list_item_task, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindTask(mTasks.get(position), mTaskClickListener, mTaskCompletionListener);
    }

    @Override
    public int getItemCount() {
        return mTasks.size();
    }
}

class ViewHolder extends RecyclerView.ViewHolder {

    private TextView taskDescriptionView;
    private CheckBox taskCompletedView;

    ViewHolder(@NonNull View itemView) {
        super(itemView);
        taskDescriptionView = itemView.findViewById(R.id.task_item_description);
        taskCompletedView = itemView.findViewById(R.id.task_item_check_box);
    }

    void bindTask(@NonNull Task task, @Nullable OnTaskClickListener taskClickListener,
                  @Nullable OnTaskCompletionListener completionListener) {
        taskDescriptionView.setText(task.getDescription());
        ViewUtil.setCheckedWithoutCallingListener(taskCompletedView, task.isCompleted());
        if (taskClickListener != null) {
            itemView.setOnClickListener(v -> taskClickListener.onTaskClick(task));
        }
        if (completionListener != null) {
            taskCompletedView.setOnCheckedChangeListener((checkBox, isCompleted) -> {
                completionListener.onChangeTaskCompletionStatusClick(task, isCompleted);
            });
        }
    }
}

class TasksDiffUtilsCallback extends DiffUtil.Callback {

    private final List<Task> oldTasks;
    private final List<Task> newTasks;

    TasksDiffUtilsCallback(@NonNull List<Task> oldTasks, @NonNull List<Task> newTasks) {
        this.oldTasks = oldTasks;
        this.newTasks = newTasks;
    }

    @Override
    public int getOldListSize() {
        return oldTasks.size();
    }

    @Override
    public int getNewListSize() {
        return newTasks.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldTasks.get(oldItemPosition).getUuid().equals(newTasks.get(newItemPosition).getUuid());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return oldTasks.get(oldItemPosition).equals(newTasks.get(newItemPosition));
    }
}