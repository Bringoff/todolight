package xyz.bringoff.todolight.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import toothpick.Scope;
import toothpick.Toothpick;

public abstract class BaseActivity extends AppCompatActivity {

    private Scope mScope;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mScope = Toothpick.openScopes(getApplication());
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, mScope);
    }
}
