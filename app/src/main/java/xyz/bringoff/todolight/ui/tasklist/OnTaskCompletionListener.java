package xyz.bringoff.todolight.ui.tasklist;

import android.support.annotation.NonNull;

import xyz.bringoff.todolight.data.Task;

public interface OnTaskCompletionListener {

    void onChangeTaskCompletionStatusClick(@NonNull Task clickedTask, boolean shouldBeCompleted);
}
