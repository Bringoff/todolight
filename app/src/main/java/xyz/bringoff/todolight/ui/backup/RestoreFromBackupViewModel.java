package xyz.bringoff.todolight.ui.backup;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.inject.Inject;

import toothpick.Scope;
import toothpick.Toothpick;
import xyz.bringoff.todolight.data.db.AppDatabase;
import xyz.bringoff.todolight.util.AppExecutors;
import xyz.bringoff.todolight.util.SingleLiveEvent;

public class RestoreFromBackupViewModel extends ViewModel {

    private AppDatabase mAppDatabase;
    private AppExecutors mAppExecutors;

    private MutableLiveData<Boolean> restoringInProgress = new MutableLiveData<>();
    private SingleLiveEvent<String> restoringFromBackupError = new SingleLiveEvent<>();
    private SingleLiveEvent<Void> restoringFromBackupSuccessful = new SingleLiveEvent<>();

    RestoreFromBackupViewModel(AppDatabase appDatabase, AppExecutors appExecutors) {
        mAppDatabase = appDatabase;
        mAppExecutors = appExecutors;
    }

    public LiveData<Boolean> getRestoringProgressStatus() {
        return restoringInProgress;
    }

    public SingleLiveEvent<String> getRestoringFromBackupError() {
        return restoringFromBackupError;
    }

    public SingleLiveEvent<Void> getRestoringFromBackupSuccessful() {
        return restoringFromBackupSuccessful;
    }

    public void onRestoreDatabaseBackupRequested(@NonNull InputStream backupInputStream) {
        String originalDatabasePath = mAppDatabase.getOpenHelper().getWritableDatabase().getPath();
        File existingDatabaseFile = new File(originalDatabasePath);

        Runnable restoreRunnable = () -> {
            restoringInProgress.postValue(true);
            OutputStream restoreBackupOutputStream = null;
            try {
                restoreBackupOutputStream = new FileOutputStream(existingDatabaseFile);
                byte[] buffer = new byte[4 * 1024];
                int read;

                while ((read = backupInputStream.read(buffer)) != -1) {
                    restoreBackupOutputStream.write(buffer, 0, read);
                }
                restoreBackupOutputStream.flush();
                restoringFromBackupSuccessful.postValue(null);
            } catch (Exception e) {
                restoringFromBackupError.postValue(e.getMessage());
            } finally {
                try {
                    backupInputStream.close();
                    if (restoreBackupOutputStream != null) {
                        restoreBackupOutputStream.close();
                    }
                } catch (IOException e) {
                    restoringFromBackupError.postValue(e.getMessage());
                }
            }
            restoringInProgress.postValue(false);
        };
        mAppExecutors.diskIO().execute(restoreRunnable);
    }
}

@SuppressWarnings("Injectable")
class RestoreFromBackupViewModelFactory extends ViewModelProviders.DefaultFactory {

    @Inject
    AppDatabase mAppDatabase;
    @Inject
    AppExecutors mAppExecutors;

    public RestoreFromBackupViewModelFactory(@NonNull Application application) {
        super(application);
        Scope scope = Toothpick.openScope(application);
        Toothpick.inject(this, scope);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (!modelClass.equals(RestoreFromBackupViewModel.class)) {
            throw new IllegalArgumentException(
                    "This factory cannot create " + modelClass.getSimpleName());
        }
        return (T) new RestoreFromBackupViewModel(mAppDatabase, mAppExecutors);
    }
}