package xyz.bringoff.todolight.ui.tasklist;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;

import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.inject.Inject;

import toothpick.Scope;
import toothpick.Toothpick;
import xyz.bringoff.todolight.data.Task;
import xyz.bringoff.todolight.data.TasksRepository;

public class TasksViewModel extends ViewModel {

    private TasksRepository mTasksRepository;


    @Inject
    public TasksViewModel(TasksRepository tasksRepository) {
        mTasksRepository = tasksRepository;
    }

    public LiveData<List<Task>> getTasks() {
        return mTasksRepository.getAllTasks();
    }

    public void onChangeTaskCompletionStatusRequested(@NonNull Task clickedTask,
                                                      boolean shouldBecameCompleted) {
        if (shouldBecameCompleted) {
            clickedTask.complete();
        } else {
            clickedTask.makeActive();
        }
        mTasksRepository.putTask(clickedTask);
    }
}

@SuppressWarnings("Injectable")
class TasksViewModelFactory extends ViewModelProviders.DefaultFactory {

    @Inject
    TasksRepository mTasksRepository;

    public TasksViewModelFactory(@NonNull Application application) {
        super(application);
        Scope scope = Toothpick.openScope(application);
        Toothpick.inject(this, scope);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (!modelClass.equals(TasksViewModel.class)) {
            throw new IllegalArgumentException(
                    "This factory cannot create " + modelClass.getSimpleName());
        }
        return (T) new TasksViewModel(mTasksRepository);
    }
}