package xyz.bringoff.todolight.ui.taskdetails;


import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import toothpick.Scope;
import toothpick.Toothpick;
import xyz.bringoff.todolight.data.Task;
import xyz.bringoff.todolight.data.TasksRepository;
import xyz.bringoff.todolight.util.SingleLiveEvent;

import static xyz.bringoff.todolight.ui.taskdetails.TaskDetailsWarnings.CHANGES_NOT_SAVED;

enum TaskDetailsWarnings {
    CHANGES_NOT_SAVED,
}

public class TaskDetailsViewModel extends ViewModel {

    public final SingleLiveEvent<Void> exitScreenNavigationCommand = new SingleLiveEvent<>();
    public final SingleLiveEvent<TaskDetailsWarnings> warningCommand = new SingleLiveEvent<>();

    private TasksRepository mTasksRepository;
    private MediatorLiveData<Task> mLiveTask = new MediatorLiveData<>();
    private boolean isDirty = false;

    public TaskDetailsViewModel(@NonNull TasksRepository tasksRepository, @Nullable String taskUuid) {
        mTasksRepository = tasksRepository;

        if (taskUuid == null) {
            mLiveTask.setValue(createNewTask());
            isDirty = true;
        } else {
            loadExistingTaskOrCreateNewIfNotFound(taskUuid);
        }
    }

    private Task createNewTask() {
        return Task.createNew();
    }

    private void loadExistingTaskOrCreateNewIfNotFound(String taskUuid) {
        LiveData<Task> liveTaskFromRepository = mTasksRepository.getTask(taskUuid);
        mLiveTask.addSource(liveTaskFromRepository, loadedTask -> {
            if (loadedTask != null) {
                mLiveTask.setValue(loadedTask);
            } else {
                isDirty = true;
                mLiveTask.setValue(createNewTask());
            }
            mLiveTask.removeSource(liveTaskFromRepository);
        });
    }

    public LiveData<Task> getTask() {
        return mLiveTask;
    }

    @MainThread
    public void onChangeDescriptionRequested(@NonNull String description) {
        Task taskToChange = mLiveTask.getValue();
        if (taskToChange != null) {
            taskToChange.changeDescription(description);
            isDirty = true;
        }
    }

    @MainThread
    public void onChangeCompletionStatusRequested(boolean completionStatus) {
        Task taskToChange = mLiveTask.getValue();
        if (taskToChange == null) {
            return;
        }
        if (completionStatus) {
            taskToChange.complete();
            isDirty = true;
        } else {
            taskToChange.makeActive();
            isDirty = true;
        }
    }

    @MainThread
    public void onSaveTaskRequested() {
        Task taskToSave = mLiveTask.getValue();
        if (taskToSave != null) {
            mTasksRepository.putTask(taskToSave);
            isDirty = false;
        }
        exitScreenNavigationCommand.call();
    }

    @MainThread
    public void onDeleteTaskRequested() {
        Task taskToDelete = mLiveTask.getValue();
        if (taskToDelete != null) {
            mTasksRepository.removeTask(taskToDelete);
            exitScreenNavigationCommand.call();
        }
    }

    @MainThread
    public void onExitRequested() {
        if (isDirty) {
            warningCommand.setValue(CHANGES_NOT_SAVED);
        }
        exitScreenNavigationCommand.call();
    }
}

@SuppressWarnings("Injectable")
class TaskDetailsViewModelFactory extends ViewModelProviders.DefaultFactory {

    @Inject
    TasksRepository mTasksRepository;
    private String mTaskUuid;

    /**
     * @param taskUuid if null new task is going to be created.
     */
    public TaskDetailsViewModelFactory(@NonNull Application application, @Nullable String taskUuid) {
        super(application);
        mTaskUuid = taskUuid;
        Scope scope = Toothpick.openScope(application);
        Toothpick.inject(this, scope);
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (!modelClass.equals(TaskDetailsViewModel.class)) {
            throw new IllegalArgumentException(
                    "This factory cannot create " + modelClass.getSimpleName());
        }
        return (T) new TaskDetailsViewModel(mTasksRepository, mTaskUuid);
    }
}