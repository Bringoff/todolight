package xyz.bringoff.todolight.ui.tasklist;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import xyz.bringoff.todolight.R;
import xyz.bringoff.todolight.data.Task;
import xyz.bringoff.todolight.ui.BaseActivity;
import xyz.bringoff.todolight.ui.backup.PerformBackupActivity;
import xyz.bringoff.todolight.ui.backup.RestoreFromBackupActivity;
import xyz.bringoff.todolight.ui.taskdetails.TaskDetailsActivity;

public class TasksActivity extends BaseActivity implements OnTaskClickListener, OnTaskCompletionListener {

    private TasksViewModel mViewModel;

    private Toolbar mToolbar;
    private RecyclerView mTaskListView;
    private TasksAdapter mTasksAdapter;
    private View mNoTasksPlaceholder;
    private FloatingActionButton mCreateNewTaskButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        initViews();

        mViewModel = ViewModelProviders.of(this, new TasksViewModelFactory(getApplication()))
                .get(TasksViewModel.class);

        mViewModel.getTasks().observe(this, tasks -> {
            assert tasks != null;
            mTasksAdapter.setNewTasks(tasks);
            mNoTasksPlaceholder.setVisibility(tasks.isEmpty() ? View.VISIBLE : View.GONE);
        });
    }

    private void initViews() {
        mToolbar = findViewById(R.id.tasks_toolbar);
        mToolbar.inflateMenu(R.menu.menu_task_list);
        mToolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.task_list_menu_backup_to_google_drive:
                    ContextCompat.startActivity(this,
                            PerformBackupActivity.getStartIntent(this), null);
                    return true;
                case R.id.task_list_menu_restore_from_google_drive:
                    ContextCompat.startActivity(this,
                            RestoreFromBackupActivity.getStartIntent(this), null);
                    return true;
            }
            return false;
        });
        mTaskListView = findViewById(R.id.tasks_list);
        mTaskListView.setLayoutManager(new LinearLayoutManager(this));
        mTasksAdapter = new TasksAdapter(this, this);
        mTaskListView.setAdapter(mTasksAdapter);
        mNoTasksPlaceholder = findViewById(R.id.tasks_empty_placeholder);

        mCreateNewTaskButton = findViewById(R.id.tasks_create_new_fab);
        mCreateNewTaskButton.setOnClickListener(v -> startCreateNewTaskScreen());
    }

    @Override
    public void onTaskClick(Task clickedTask) {
        startShowExistingTaskScreen(clickedTask);
    }

    private void startCreateNewTaskScreen() {
        Intent detailsIntent = TaskDetailsActivity.getCreateNewIntent(this);
        ContextCompat.startActivity(this, detailsIntent, null);
    }

    private void startShowExistingTaskScreen(Task task) {
        Intent detailsIntent = TaskDetailsActivity.getViewDetailsIntent(this, task.getUuid());
        ContextCompat.startActivity(this, detailsIntent, null);
    }

    @Override
    public void onChangeTaskCompletionStatusClick(@NonNull Task clickedTask, boolean shouldBeCompleted) {
        mViewModel.onChangeTaskCompletionStatusRequested(clickedTask, shouldBeCompleted);
    }
}
