package xyz.bringoff.todolight.ui.tasklist;

import xyz.bringoff.todolight.data.Task;

public interface OnTaskClickListener {

    void onTaskClick(Task clickedTask);
}
