package xyz.bringoff.todolight.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import javax.inject.Singleton;

@Singleton
@Database(entities = {TaskEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public static final String DATABASE_NAME = "todolight.db";

    public abstract TaskDao tasksDao();

    public void replaceLocalDatabase() {

    }
}
