package xyz.bringoff.todolight.data;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

public interface TasksRepository {

    LiveData<List<Task>> getAllTasks();

    /**
     * @return android.arch.lifecycle.LiveData with desired task.
     * Value can be null if task doesn't exist.
     */
    LiveData<Task> getTask(@NonNull String uuid);

    void putTask(@NonNull Task task);

    void removeTask(@NonNull Task task);
}
