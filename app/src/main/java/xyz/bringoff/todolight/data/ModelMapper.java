package xyz.bringoff.todolight.data;


public interface ModelMapper<Model, DbEntity> {

    Model fromDbEntity(DbEntity dbEntity);

    DbEntity toDbEntity(Model model);
}
