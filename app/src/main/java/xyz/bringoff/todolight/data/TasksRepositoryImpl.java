package xyz.bringoff.todolight.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.annimon.stream.Stream;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import xyz.bringoff.todolight.util.AppExecutors;
import xyz.bringoff.todolight.data.db.TaskDao;
import xyz.bringoff.todolight.data.db.TaskEntity;

@Singleton
public class TasksRepositoryImpl implements TasksRepository {

    private AppExecutors mAppExecutors;
    private TaskDao mTaskDao;

    private LiveData<List<Task>> mLiveTasks;
    private ModelMapper<Task, TaskEntity> mTaskMapper = new TaskMapper();

    @Inject
    TasksRepositoryImpl(@NonNull AppExecutors appExecutors, @NonNull TaskDao taskDao) {
        mAppExecutors = appExecutors;
        mTaskDao = taskDao;
        mLiveTasks = Transformations.map(mTaskDao.loadAllTasks(), input -> Stream.of(input)
                .map(taskEntity -> mTaskMapper.fromDbEntity(taskEntity))
                .toList());
    }

    @NonNull
    @Override
    public LiveData<List<Task>> getAllTasks() {
        return mLiveTasks;
    }

    @Nullable
    @Override
    public LiveData<Task> getTask(@NonNull String uuid) {
        return Transformations.map(mTaskDao.loadTaskByUuid(uuid),
                input -> mTaskMapper.fromDbEntity(input));

    }

    @Override
    public void putTask(@NonNull final Task taskToPut) {
        Runnable saveRunnable = () -> {
            TaskEntity dbEntity = mTaskMapper.toDbEntity(taskToPut);
            mTaskDao.insertTasks(dbEntity);
        };
        mAppExecutors.diskIO().execute(saveRunnable);
    }

    @Override
    public void removeTask(@NonNull Task taskToRemove) {
        Runnable removeRunnable = () -> {
            TaskEntity dbEntity = mTaskMapper.toDbEntity(taskToRemove);
            mTaskDao.deleteTasks(dbEntity);
        };
        mAppExecutors.diskIO().execute(removeRunnable);
    }
}
