package xyz.bringoff.todolight.data;

import xyz.bringoff.todolight.data.db.TaskEntity;

public class TaskMapper implements ModelMapper<Task, TaskEntity> {

    @Override
    public Task fromDbEntity(TaskEntity taskEntity) {
        return new Task(taskEntity.uuid, taskEntity.description, taskEntity.isCompleted);
    }

    @Override
    public TaskEntity toDbEntity(Task task) {
        return new TaskEntity(task.getUuid(), task.getDescription(), task.isCompleted());
    }
}
