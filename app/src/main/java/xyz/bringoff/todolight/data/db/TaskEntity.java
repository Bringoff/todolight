package xyz.bringoff.todolight.data.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "tasks")
public class TaskEntity {

    @PrimaryKey
    @NonNull
    public final String uuid;

    @NonNull
    public final String description;

    @ColumnInfo(name = "completed")
    public final boolean isCompleted;

    public TaskEntity(String uuid, String description, boolean isCompleted) {
        this.uuid = uuid;
        this.description = description;
        this.isCompleted = isCompleted;
    }
}
