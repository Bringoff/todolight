package xyz.bringoff.todolight.data;


import android.support.annotation.NonNull;

import java.util.UUID;

public class Task {

    private final String uuid;

    private String mDescription;

    private boolean isCompleted;

    public Task(String uuid, String mDescription) {
        this(uuid, mDescription, false);
    }

    public Task(String uuid, String mDescription, boolean isCompleted) {
        this.uuid = uuid;
        this.mDescription = mDescription;
        this.isCompleted = isCompleted;
    }

    public static Task createNew() {
        return new Task(UUID.randomUUID().toString(), "", false);
    }

    public static Task createNew(@NonNull String description) {
        return new Task(UUID.randomUUID().toString(), description, false);
    }


    public String getUuid() {
        return uuid;
    }

    public String getDescription() {
        return mDescription;
    }

    public void changeDescription(@NonNull String description) {
        this.mDescription = description;
    }

    public void complete() {
        isCompleted = true;
    }

    public void makeActive() {
        isCompleted = false;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public boolean isActive() {
        return !isCompleted();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (isCompleted != task.isCompleted) return false;
        return mDescription != null ? mDescription.equals(task.mDescription) : task.mDescription == null;
    }

    @Override
    public int hashCode() {
        int result = mDescription != null ? mDescription.hashCode() : 0;
        result = 31 * result + (isCompleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "mDescription='" + mDescription + '\'' +
                ", isCompleted=" + isCompleted +
                '}';
    }
}
