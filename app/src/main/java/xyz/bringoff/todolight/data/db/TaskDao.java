package xyz.bringoff.todolight.data.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import java.util.List;

@Dao
public interface TaskDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTasks(TaskEntity task, TaskEntity... tasks);

    @Delete
    void deleteTasks(TaskEntity task, TaskEntity... tasks);

    @Query("SELECT * FROM tasks")
    LiveData<List<TaskEntity>> loadAllTasks();

    @Query("SELECT * FROM tasks WHERE uuid = :uuid")
    LiveData<TaskEntity> loadTaskByUuid(@NonNull String uuid);
}
