package xyz.bringoff.todolight.data.db;

import javax.inject.Inject;
import javax.inject.Provider;

public class TaskDaoProvider implements Provider<TaskDao> {

    private AppDatabase mAppDatabase;

    @Inject
    public TaskDaoProvider(AppDatabase appDatabase) {
        mAppDatabase = appDatabase;
    }

    @Override
    public TaskDao get() {
        return mAppDatabase.tasksDao();
    }
}
