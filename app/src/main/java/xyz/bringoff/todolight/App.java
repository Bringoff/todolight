package xyz.bringoff.todolight;

import android.app.Application;

import toothpick.Scope;
import toothpick.Toothpick;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initDi();
    }

    private void initDi() {
        Scope appScope = Toothpick.openScope(this);
        appScope.installModules(new AppModule(this));
    }
}
